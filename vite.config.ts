import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'


// https://vitejs.dev/config/
// export default defineConfig({
//   plugins: [vue()],
// })


export default defineConfig({
  plugins: [
  vue(),
  ],
  resolve: {
    alias: {
      '@am': fileURLToPath(new URL('./node_modules/ambient/src', import.meta.url))
    },
    preserveSymlinks: true,
  },
  base: '/vstock/'
})
