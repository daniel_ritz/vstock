import { Photographer } from "./Photographer"
import { Server } from "./Server"

const photographersUpdatedHandlers = <{(): void}[]>[]
const tagsUpdatedHandlers = <{(): void}[]>[]

export function onPhotographersUpdated(callback: {(): void}) {
    photographersUpdatedHandlers.push(callback)
}

export function onTagsUpdated(callback: {(): void}) {
    tagsUpdatedHandlers.push(callback)
}

export function getTags(): string[] {
    const tags = localStorage.getItem('vstock::tags')
    const data = tags ? JSON.parse(tags) : null
    return data ?? []
}

export function saveTag(tag: string) {
    const tags = getTags()
    if (!tags.includes(tag)) tags.push(tag)
    localStorage.setItem('vstock::tags', JSON.stringify(tags))
    tagsUpdatedHandlers.forEach(h => h())
}

export function forgetTag(tag: string) {
    const tags = getTags().filter(t => t != tag)
    localStorage.setItem('vstock::tags', JSON.stringify(tags))
    tagsUpdatedHandlers.forEach(h => h())
}

export function getServers(): {[key: string]: Server} {
    const data = localStorage.getItem('vstock::servers')
    const servers = data ? JSON.parse(data) : null
    return servers ?? {}
}

export function saveServer(server: Server) {
    const servers = getServers()
    servers[server.url] = server
    localStorage.setItem('vstock::servers', JSON.stringify(servers))
}

export function forgetServer(server: Server) {
    const servers = getServers()
    delete servers[server.url]
    localStorage.setItem('vstock::servers', JSON.stringify(servers))
} 

export function getDiscoveryServer() {
    const serverUrl = localStorage.getItem('vstock::discovery-server')
    if (serverUrl == null) return null
    const servers = getServers()
    return serverUrl in servers ? servers[serverUrl] : null
}

export function saveDiscoveryServer(server?: Server) {
    const serverUrl = server?.url ?? null
    if (serverUrl == null) localStorage.removeItem('vstock::discovery-server')
    else localStorage.setItem('vstock::discovery-server', serverUrl)
}

export function getPhotographers(): Photographer[] {
    const data = localStorage.getItem('vstock::photographers')
    const photographers = data ? JSON.parse(data) : null
    return photographers ?? []
}

export function savePhotographer(photographer: Photographer) {
    const photographers = getPhotographers()
    if (photographers.map(p => p.name).includes(photographer.name)) return
    photographers.push(photographer)
    localStorage.setItem('vstock::photographers', JSON.stringify(photographers))
    photographersUpdatedHandlers.forEach(h => h())
}

export function forgetPhotographer(photographer: Photographer) {
    const photographers = getPhotographers().filter(p => p.name != photographer.name)
    localStorage.setItem('vstock::photographers', JSON.stringify(photographers))
    photographersUpdatedHandlers.forEach(h => h())
}

export function getPreferredLanguage() {
    return localStorage.getItem('vstock::language')
}

export function setPreferredLanguage(lang: string | null) {
    if (lang == null) {
        localStorage.removeItem('vstock::language')
    } else {
        localStorage.setItem('vstock::language', lang)
    }
}

export function getPreferredView(): 'landscape'|'portrait'|'square' {
    const data = localStorage.getItem('vstock::view')
    if (data == null || !['landscape','portrait','square'].includes(data)) {
        return 'portrait'
    }
    return data as any
}

export function setPreferredView(view: 'landscape'|'portrait'|'square') {
    localStorage.setItem('vstock::view', view)
}

export function getOnboardingState(): string {
    const data = localStorage.getItem('vstock::onboarding')
    return data ?? ""
}

export function setOnboardingState(state: string) {
    localStorage.setItem('vstock::onboarding', state)
}