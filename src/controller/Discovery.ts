import { Photographer, transformAccount } from "./Photographer";
import { getDiscoveryServer } from "./Preferences";
import { loginToServer, Server } from "./Server";

export async function searchPhotographers(query: string): Promise<Photographer[]> {
    const discoveryServer = getDiscoveryServer()
    if (discoveryServer == null) return []

    const client = await loginToServer(discoveryServer)
    const result = await client.v2.search({
        q: query,
        type: 'accounts',
        resolve: false,
    })

    return result.accounts.map(a => transformAccount(a, discoveryServer.url))
}

export async function loadPhotographerInfo(server: Server, username: string): Promise<Photographer> {
    const client = await loginToServer(server)
    const result = await client.v2.search({
        q: username,
        type: 'accounts',
        resolve: false,
    })

    if (result.accounts.length == 0) throw "Account not found"
    return transformAccount(result.accounts[0], server.url)
}