import * as masto from 'masto'
import { longUrl, Server } from './Server'

const availableClients: {[key: string]: masto.mastodon.Client} = {}

export async function connectClient(server: Server) {
    const url = longUrl(server.url)
    if (url == null) throw 'Server has no url'
    const client = await masto.login({
        url,
        disableVersionCheck: true
    })
    availableClients[server.url] = client
}

export function getClient(serverUrl: string): masto.mastodon.Client {
    if (!(serverUrl in availableClients)) throw `Server ${serverUrl} is not availabe`
    return availableClients[serverUrl]
}