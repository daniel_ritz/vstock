export type License = 'C' | 'CC0' | 'CC BY-SA' | 'CC BY' | ''

export function shortTitle(license: License): string {
    switch(license) {
        case 'CC0':
            return 'CC0';
        case 'CC BY':
            return 'BY';
        case 'CC BY-SA':
            return 'BY-SA';
        default:
            return '(C)';
    }
}

export function longTitle(license: License): string {
    switch(license) {
        case 'CC0':
            return 'Public Domain / No Rights Reserved';
        case 'CC BY':
            return 'Attribution';
        case 'CC BY-SA':
            return 'Attribution, Share-Alike';
        case 'C':
            return 'All Rights Reserved'
        default:
            return license == '' ? 'All Rights Reserved' : license;
    }
}

export function longTitleKey(license: License): string {
    if (license.substring(0, 1) == 'C') {
        return license
    } else {
        return 'C'
    }
}

export function findLicense(text: string): License | undefined {
    const licenses = <License[]>['CC BY-SA', 'CC BY', 'CC0']
    for (const license of licenses) {
        if (text.includes(license)) return license
    }
    return undefined
}

export function normalizeLicense(license: License): License {
    if (['CC0', 'CC BY-SA', 'CC BY', 'C'].includes(license)) return license
    else return 'C'
}

export function compatibleLicense(_query: License, targetLicense: License) {
    const query = normalizeLicense(_query)
    if (targetLicense == 'C') {
        return true
    } else if (targetLicense == 'CC BY-SA') {
        if (query == 'C') return false
        else return true
    } else if (targetLicense == 'CC BY') {
        if (query == 'C' || query == 'CC BY-SA') return false
        else return true
    } else if (targetLicense == 'CC0') {
        return query == 'CC0'
    }
}