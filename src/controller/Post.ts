import { Photographer, transformAccount } from "./Photographer"
import * as masto from 'masto';
import { findLicense, License } from "./License";

export type Post = {
    id: string,
    attachment: Media,
    content: string,
    photographer: Photographer
    reblog: boolean,
    server: string,
}

export type Media = {
    blurhash?: string,
    description?: string,
    meta?: any,
    preview: string,
    url?: string,
    license: License | "",
}

export function transformStatus(status: masto.mastodon.v1.Status, server: string): Post[] {
    const attachments = transformAttachments(status.mediaAttachments)
    const assumedLicense = findLicense(status.content) ?? ""
    return attachments.map(attachment => {
        let a = attachment
        if (a.license == "" && assumedLicense != "") a.license = assumedLicense
        return {
            id: status.id,
            attachment: a,
            content: status.content,
            photographer: transformAccount(status.account, server),
            reblog: status.reblog ? true : false,
            server,
        }
    })
}

export function transformAttachments(attachments: masto.mastodon.v1.MediaAttachment[]): Media[] {
    return attachments.filter(a => a.type == 'image').map(transformAttachment)
}

export function transformAttachment(attachment: masto.mastodon.v1.MediaAttachment): Media {
    let license = ""
    if ('license' in attachment) {
        license = typeof attachment.license == 'object' 
                    ? (attachment.license as any).name
                    : attachment.license
    } else if (attachment.meta != null && 'license' in attachment.meta) {
        license = typeof attachment.meta.license == 'object' 
                    ? (attachment.meta.license as any).name
                    : attachment.meta.license
    }
    return {
        blurhash: attachment.blurhash ?? undefined,
        description: attachment.description ?? undefined,
        meta: attachment.meta,
        preview: attachment.previewUrl,
        url: attachment.url ?? undefined,
        license: license as License
    }
}