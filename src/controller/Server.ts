import { login } from "masto"
import { saveServer } from "./Preferences"
import { connectClient } from "./ServerRepository"

export type ServerType = 'mastodon' | 'pixelfed' | 'pleroma'

export type Server = {
    url: string,
    name: string,
    token?: string,
    client_id?: string,
    client_secret?: string,
    type: ServerType,
    needsAuthentication: boolean,
}

const urlRegex = /^(https?:\/\/)?([a-zA-Z0-9\-\.]+\.[a-zA-Z0-9\-]+)$/

export function shortUrl(url: string) {
    const match = url.match(urlRegex)
    if (match == null) return null
    return match[2]
}

export function longUrl(url: string) {
    const match = url.match(urlRegex)
    if (match == null) return null
    // only allow https!
    return "https://" + match[2]
}

export function validateUrl(url: string) {
    return url.match(urlRegex) !== null
}

export async function getServerInfo(url: string) {
    if (!validateUrl(url)) throw "Invalid URL"
    const fullUrl = longUrl(url)!
    const nameUrl = shortUrl(url)!

    const masto = await login({
        url: fullUrl,
        disableVersionCheck: true,
    })

    const instanceInfo = await masto.v1.instances.fetch()
    const version = instanceInfo.version
    let title = instanceInfo.title
    const type = getServerType(instanceInfo.version)

    if (['mastodon', 'pixelfed', 'pleroma'].includes(title.toLowerCase().trim())) {
        title = url
    }

    const server = <Server>{
        url: nameUrl,
        name: title,
        type,
        needsAuthentication: type == 'pixelfed' ? true : false
    }

    return server
}

function getServerType(version: string): ServerType {
    const v = version.toLowerCase()
    if (v.includes('pixelfed')) return 'pixelfed'
    if (v.includes('pleroma')) return 'pleroma'
    return 'mastodon'
}

export async function registerServer(url: string) {
    const server = await getServerInfo(url)
    saveServer(server)
    connectClient(server)
    return server
}

export function loginToServer(server: Server) {
    return login({
        url: longUrl(server.url)!,
        disableVersionCheck: true,
    })
}