import { Photographer } from "./Photographer";
import { Post, transformStatus } from "./Post";
import { getClient } from "./ServerRepository";

export class Timeline {
    private photographers: Photographer[]

    constructor(photographers: Photographer[]) {
        this.photographers = photographers
    }

    public loadCollection(tag: string, options: {olderThanId?: {[key: string]: string}}, callback: {(posts: Post[], photographer: Photographer, success: boolean, lastResult: boolean): void}) {
        const expectedResponses = this.photographers.length
        let responseCount = 0
        this.photographers.forEach(p => {
            try {
                const client = getClient(p.server)
                client.v1.accounts.listStatuses(p.acc, {
                    onlyMedia: true,
                    tagged: tag.substring(1),
                    limit: 50,
                    maxId: options.olderThanId ? options.olderThanId[p.name] : undefined,
                }).then(statuses => {
                    const posts = statuses.map(s => transformStatus(s, p.server)).flat()
                    responseCount++
                    callback(posts, p, true, responseCount == expectedResponses)
                })
            } catch(_: any) {
                console.log('err', _)
                responseCount++
                callback([], p, false, responseCount == expectedResponses)
            }
        })
    }

    public loadTags(term: string, callback: {(tags: string[], term: string, success: boolean, lastResult: boolean): void}) {
        const servers = this.photographers
            .map(p => p.server)
            .reduce((p: string[], c, _) => {
                if (p.includes(c)) return p
                else return [...p, c]
            }, [])
        const expectedResponses = servers.length
        let responseCount = 0
        servers.forEach(s => {
            try {
                const client = getClient(s)
                client.v2.search({
                    q: term,
                    type: 'hashtags',
                    resolve: false,
                }).then(result => {
                    const tags = result.hashtags.map(t => `#${t.name}`)
                    responseCount++
                    callback(tags, term, true, responseCount == expectedResponses)
                })
            } catch(_: any) {
                responseCount++
                callback([], term, false, responseCount == expectedResponses)
            }

        })
    }

    public loadProfile(photographer: Photographer, options: {olderThanId?: string}, callback: {(posts: Post[], success: boolean): void}) {
        const client = getClient(photographer.server)
        try {
            client.v1.accounts.listStatuses(photographer.acc, {
                onlyMedia: true,
                excludeReplies: true,
                maxId: options.olderThanId,
                limit: 50,
            }).then(statuses => {
                callback(statuses.map(s => transformStatus(s, photographer.server)).flat(), true)
            })
        } catch(_: any) {
            callback([], false)
        }
    }

}