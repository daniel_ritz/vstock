import * as masto from 'masto';
import { Server } from './Server';

export type Photographer = {
    name: string,
    acc: string,
    avatar: string,
    displayName: string,
    profile: string,
    posts: number,
    server: string,
}

export function transformAccount(account: masto.mastodon.v1.Account, server: string): Photographer {
    const name = account.acct.includes('@')
        ? '@' + account.acct
        : '@' + account.acct + '@' + server
    return {
        name,
        acc: account.id,
        avatar: account.avatarStatic,
        displayName: account.displayName,
        profile: account.note,
        posts: account.statusesCount,
        server
    }
}

