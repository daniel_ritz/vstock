import { createApp, ref } from 'vue'
import 'ambient/src/theme/theme.scss'
import './theme.scss'
import App from './App.vue'
import enLanguage from './l18n/en.json'
import deLanguage from './l18n/de.json'
import { getOnboardingState, getPreferredLanguage } from './controller/Preferences'
import { AmbientAppBuilder } from '@am/traits/AmbientApp'
import HomeViewVue from './views/HomeView.vue'
import CollectionViewVue from './views/CollectionView.vue'
import DetailViewVue from './views/DetailView.vue'
import FindPhotographerViewVue from './views/FindPhotographerView.vue'
import SettingsViewVue from './views/SettingsView.vue'
import ProfileViewVue from './views/ProfileView.vue'
import OnboardingViewVue from './views/OnboardingView.vue'
import AddToHomescreenVue from './views/onboarding/AddToHomescreen.vue'
import { pushContext } from '@am/traits/NavigationGraph'
import ConnectPhotographerVue from './views/findPhotographer/ConnectPhotographer.vue'
import SelectPhotographerServerVue from './views/findPhotographer/SelectPhotographerServer.vue'
import SearchPhotographerVue from './views/findPhotographer/SearchPhotographer.vue'
import OnboardingServerVue from './views/onboarding/OnboardingServer.vue'
import OnboardingWelcomeVue from './views/onboarding/OnboardingWelcome.vue'
import OnboardingLicensesVue from './views/onboarding/OnboardingLicenses.vue'
import EmptyViewVue from './views/EmptyView.vue'
import pkg from '../package.json'

const language = getPreferredLanguage() ?? navigator.language
const languagePack = enLanguage

if (/^de/.test(language)) {
    Object.assign(languagePack, deLanguage)
}

const builder = new AmbientAppBuilder(App)
builder.provideL18n(ref(languagePack))
builder.resolveView((view) => {
    switch(view) {
        case 'home':
            return HomeViewVue
        case 'collection':
            return CollectionViewVue
        case 'detail':
            return DetailViewVue
        case 'find-photographer':
            return FindPhotographerViewVue
        case 'settings':
            return SettingsViewVue
        case 'profile':
            return ProfileViewVue
        case 'add-to-homescreen':
            return AddToHomescreenVue
        case 'search-photographer':
            return SearchPhotographerVue
        case 'select-photographer-server':
            return SelectPhotographerServerVue
        case 'connect-photographer':
            return ConnectPhotographerVue
        case 'onboarding':
            return OnboardingViewVue
        case 'onboarding-welcome':
            return OnboardingWelcomeVue
        case 'onboarding-server':
            return OnboardingServerVue
        case 'onboarding-licenses':
            return OnboardingLicensesVue
        case 'empty':
            return EmptyViewVue
    }
})
builder.restoreFromPath(() => {
    pushContext({
        view: 'home',
    }, [{ hint: 'places', emptyView: 'empty' }])
    checkOnboarding()
})
builder.addTrait({
    provide: {
        appinfo: {
            name: pkg.name,
            version: pkg.version,
            url: pkg.homepage,
        }
    }
})

const app = builder.build()
app.mount('#app')

function checkOnboarding() {
    if (getOnboardingState() == "1.0.0") return
    pushContext({
        view: 'onboarding-welcome'
    }, [{ hint: 'dialog', scaffolding: 'onboarding', noCancel: true }])
    if (navigator.platform == 'iPhone' || navigator.platform == 'iPad') {
        if (!(navigator as any).standalone) {
            pushContext({
                view: 'add-to-homescreen'
            })
        }
    }
}

